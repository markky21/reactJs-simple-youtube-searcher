import React, {Component} from "react";
import ReactDOM from "react-dom";
import YTSearch from "youtube-api-search";
import _ from 'lodash';

import SearchBar from "./components/search_bar";
import VideoList from "./components/video_list";
import VideoDetail from "./components/video_detail";

const API_KEY = 'AIzaSyDjkqIYvWbbttR3oXeaN05ZBQi1aaumGF8';


class App extends Component {

    constructor(props) {
        super(props);

        this.state = {
            videos: [],
            selectedVideo: null
        };

        this.searchVideos('Maddi Jane');
    }

    searchVideos(term) {
        YTSearch({key: API_KEY, term: term}, (videos) => {
            this.setState({
                videos: videos,
                selectedVideo: videos[0]
            });
            console.log(videos);
        });
    }

    render() {

        const videoSearch = _.debounce((term) => this.searchVideos(term), 300);

        return (
            <div className='row'>
                <div className="col-12 mt-5">
                    <header className="row mb-5">
                        <div className="col-12">
                            <SearchBar onSearchTermChange={(term) => videoSearch(term)} />
                        </div>
                    </header>

                    <div className="row mb-5">
                        <section className="col-8 col-md-8 ">
                            <VideoDetail video={this.state.selectedVideo}/>
                        </section>
                        <section className="col-4 col-md-4 ">
                            <VideoList
                                onVideoClick={selectedVideo => this.setState({selectedVideo})}
                                videos={this.state.videos}/>
                        </section>
                    </div>
                </div>
            </div>
        );
    }

}

ReactDOM.render(<App/>, document.querySelector('.container'));