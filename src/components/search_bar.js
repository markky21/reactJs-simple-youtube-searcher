import React, {Component} from "react";

class SearchBar extends Component {

    constructor(props) {
        super(props);

        this.state = {term: ''};
    }

    onInputChange(term) {
        // .props.term({term: event.target.value})
        this.props.onSearchTermChange(term);
        this.setState({term});
    }

    render() {
        return (
            <nav>
                <form>
                    <div class="form-group">
                        <input type="text"
                               placeholder="Search a video"
                               value={this.state.term}
                               className="form-control"
                               onChange={(event) => this.onInputChange(event.target.value)}
                        />
                    </div>
                </form>
            </nav>
        )
    }

}


export default SearchBar;