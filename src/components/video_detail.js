import React from "react"

const VideoDetail = ({video}) => {

    if(!video) {
        return (<div>Loading...</div>)
    }

    const title = video.snippet.title;
    const desc = video.snippet.description;
    const url = `https://www.youtube.com/embed/${video.id.videoId}`;

    return (
        <div className="video-detail">
            <div className="embed-responsive embed-responsive-16by9 mb-3">
                <iframe className="embed-responsive-item" src={url} frameborder="0"></iframe>
            </div>
            <h2 className='mb-3'>{title}</h2>
            <p className='mb-5'>{desc}</p>
        </div>
    );
};

export default VideoDetail