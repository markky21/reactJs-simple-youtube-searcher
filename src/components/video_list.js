import React, {Component} from "react"
import VideoListItem from './video_list_item';

const VideoList = (props) => {

    const videoItems = props.videos.map((video) => {
        return (
            <li className='list-group-item p-2' key={video.id.videoId}>
                <VideoListItem
                    onVideoClick={props.onVideoClick}
                    video={video}
                />
            </li>
        );
    });

    return (
        <ul className="list-group">
            {videoItems}
        </ul>
    )
}

export default VideoList