import React from 'react';

const VideoListItem = ({video, onVideoClick}) => {

    const imageUrl = video.snippet.thumbnails.high.url;
    const title = video.snippet.title;

    return (
        <article className='video-list media' onClick={() => onVideoClick(video)}>
            <div className="media-left" style={{width: '45%'}}>
                <img className="card-img-top" src={imageUrl} alt="Card image cap"/>
            </div>
            <div className="media-body p-2">
                <h6 className="media-headding font-small">{title}</h6>
            </div>
        </article>
    );
};

export default VideoListItem;